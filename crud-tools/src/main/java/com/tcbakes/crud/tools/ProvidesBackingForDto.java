package com.tcbakes.crud.tools;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.tcbakes.crud.spec.base.Dto;

/**
 * Marks a Class as one that provides a backing object for a {@link Dto}.
 * 
 * @author Tristan Baker
 * @since 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ProvidesBackingForDto {

    /**
     * @return The Dto class whose backing is being provided for.
     */
    Class<?> value();

    /**
     * @return Optional. Context that can be used to disambiguate two different classes
     *         that provide backing for the same Dto.
     */
    String context() default "default";
}
