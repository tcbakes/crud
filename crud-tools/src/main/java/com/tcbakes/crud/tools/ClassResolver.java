package com.tcbakes.crud.tools;

import org.springframework.util.ClassUtils;

public class ClassResolver {

	public Class<?> resolveClassName(String className, ClassLoader classLoader) {
		return ClassUtils.resolveClassName(className, classLoader);
	}

}
