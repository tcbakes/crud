package com.tcbakes.crud.tools2.constructedclasses;

public class ConstructedBar {
	
	Long _id;
	
	public ConstructedBar(Long id) {
		_id = id;
	}
	
	public Long getId() {
		return _id;
	}

	public void setId(Long _id) {
		this._id = _id;
	}
}
