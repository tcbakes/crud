package com.tcbakes.crud.tools2.testclasses;

/**
 * Created by tbaker on 9/4/15.
 */
public class Baz {
    String alpha;

    public String getAlpha() {
        return alpha;
    }

    public void setAlpha(String alpha) {
        this.alpha = alpha;
    }
}
