package com.tcbakes.crud.tools2;

import com.tcbakes.crud.tools.InitializableFrom;

public abstract class AbstractFakeItem implements InitializableFrom<FakeItemDto,Long>{


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	private Long id;
}
