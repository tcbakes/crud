package com.tcbakes.crud.tools2;

import com.tcbakes.crud.tools.InitializableFrom;
import com.tcbakes.crud.tools.ProvidesBackingForDto;

@ProvidesBackingForDto(value=FauxItemDto.class, context = "custom")
public class FauxItem implements InitializableFrom<FauxItemDto, Long> {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    Long id;

    @Override
    public void initializeFrom(FauxItemDto dto) {
        this.setId(dto.getId());
    }

}
