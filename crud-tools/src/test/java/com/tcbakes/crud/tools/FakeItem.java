package com.tcbakes.crud.tools;

@ProvidesBackingForDto(FakeItemDto.class)
public class FakeItem extends AbstractFakeItem {

	private String name;
	
	@Override
	public void initializeFrom(FakeItemDto dto) {

		this.name = dto.getName();
		this.setId(dto.getId());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
