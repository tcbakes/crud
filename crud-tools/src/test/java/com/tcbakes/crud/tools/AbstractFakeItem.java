package com.tcbakes.crud.tools;

public abstract class AbstractFakeItem implements InitializableFrom<FakeItemDto,Long>{


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	private Long id;
}
