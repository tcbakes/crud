package com.tcbakes.crud.impl.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;


public class CrudItemValidationException extends AbstractException {

	private static final long serialVersionUID = 1L;
	
	private Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
	private Status status;
	
	public CrudItemValidationException() {
		super();
	}

	public CrudItemValidationException(String simpleReason) {
		super(simpleReason);
		this.status = Status.BAD_REQUEST;
	}
	
	public CrudItemValidationException(String simpleReason, Status status) {
	    super(simpleReason);
	    this.status = status;
	}

	public CrudItemValidationException(ValidationFailures failures) {
		super(failures != null ? failures.toString() : "A validation exception has occurred");
		
		this.fieldErrors = failures.getFieldErrors();
	}
	
	public CrudItemValidationException(ValidationFailures failures, Status status) {
        super(failures != null ? failures.toString() : "A validation exception has occurred");
        
        this.fieldErrors = failures.getFieldErrors();
        this.status = status;
    }

    public Map<String, List<String>> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, List<String>> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }	
}
