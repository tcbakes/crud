package com.tcbakes.crud.impl.exception;

public class CredentialVerificationException extends AbstractException{

	private static final long serialVersionUID = 1L;

	public CredentialVerificationException(){
		super();
	}
	
	public CredentialVerificationException(String msg){
		super(msg);
	}
}
