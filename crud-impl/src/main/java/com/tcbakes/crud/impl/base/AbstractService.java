package com.tcbakes.crud.impl.base;

import javax.ejb.EJB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcbakes.crud.impl.exception.CrudItemNotFoundException;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.spec.exception.AbstractSpecException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.AllExcpetionsMapper;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.ValidationException;

public class AbstractService {

	private ContextProvider provider = null;

	/**
	 * @return The {@link ContextProvider}, which in turn provides access to the
	 *         {@link ExecutionContext}
	 */
	protected ContextProvider getContextProvider() {
		if (provider == null)
			provider = new SimpleThreadLocalContextProvider();

		return provider;
	}
	
	/**
	 * @return The {@link ExecutionContext}, which provides access to useful
	 *         context objects required for implementing classes to do
	 *         interesting things (managed transactions, access persistent
	 *         storage, get the current user, etc)
	 */
	protected ExecutionContext getContext() {
		return getContextProvider().getContext();
	}

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@EJB
	protected TransactionAccessLocal txnAccess;

	protected void setTxnAccess(TransactionAccessLocal txnAccess) {
		this.txnAccess = txnAccess;
	}

	/**
	 * Wraps the provided logic with exception handling that will translate an
	 * exception from the com.tcbakes.crud.impl.exception.* package into the
	 * equivalent com.tcbakes.crud.spec.exception.* exception. Additionally,
	 * this method is responsible for establishing the ExectionContext available
	 * through {@link #getContext()} and through which all extending classes
	 * should be accessing Persistence Managers, current Users, etc.
	 * 
	 * @param l
	 *            the logic to wrap
	 * @return the result of the execution
	 * @throws BadRequestException
	 *             if the execution throws a {@link CrudIllegalArgException}
	 * @throws NotFoundException
	 *             if the execution throws a {@link CrudItemNotFoundException}
	 * @throws com.tcbakes.crud.spec.exception.ValidationException
	 *             if the execution throws a {@link CrudItemValidationException}
	 * @throws AccessForbiddenException
	 *             if the execution throws a
	 *             {@link CrudAccessForbiddenException}
	 * @throws ServiceException
	 *             if any other kind of exception is thrown
	 */
	protected <T> T wrapAndExecute(ExceptionHandledLogic<T> l) {
		try {

			getContext().setTxnAccss(this.txnAccess);

			return l.execute();

		} catch (CrudItemNotFoundException ex) {
			NotFoundException nfe = new NotFoundException(ex.getMessage(), ex);
			logger.info(AllExcpetionsMapper.exceptionAsJson(nfe));
			throw nfe;
		} catch (CrudItemValidationException ex) {
			ValidationException vex = new ValidationException(
					ex.getMessage(), ex.getFieldErrors());
			logger.info(AllExcpetionsMapper.exceptionAsJson(vex));
			throw vex;
		} catch (AbstractSpecException ex) {
			logger.info(AllExcpetionsMapper.exceptionAsJson(ex));
			throw ex;
		} catch (Exception ex) {
			String message = "Something went wrong. Operation cannot complete.";
			this.logger.error(message, ex);
			throw new ServiceException(message, ex);
		} finally {
			getContextProvider().unset();
		}
	}

	/**
	 * Logic that will be wrapped with universally useful exception handling.
	 * 
	 * @author Tristan Baker
	 * 
	 * @param <T>
	 *            The type of the item returned by the execute method
	 */
	protected interface ExceptionHandledLogic<T> {
		public T execute();
	}
}
