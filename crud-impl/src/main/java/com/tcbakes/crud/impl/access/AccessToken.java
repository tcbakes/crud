package com.tcbakes.crud.impl.access;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PrePersist;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import com.tcbakes.crud.impl.base.Crud;
import com.tcbakes.crud.impl.base.TransactionWrappedLogic;
import com.tcbakes.crud.spec.access.AccessTokenDto;
import com.tcbakes.crud.spec.exception.AccessUnauthorizedException;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.tools.ProvidesBackingForDto;

/**
 * An AccessToken is issued to a User upon validation of their credentials.
 * 
 * @author Tristan Baker
 */
@ProvidesBackingForDto(AccessTokenDto.class)
public class AccessToken extends Crud<AccessToken, AccessTokenDto, String> {

    private static final long serialVersionUID = 1L;

    String id;
    Calendar start;
    Calendar mostRecentlyActive;
    Boolean enabled;
    Integer allowableInactiveSeconds;

    /**
     * The user to which this AccessToken is granted.
     */
    User user;

    /*
     * (non-Javadoc)
     * 
     * @see com.tcbakes.crud.impl.base.Crud#getId()
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The time that this AccessToken was issued
     */
    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    /**
     * @return The time that this AccessToken was last used to perform some
     *         operation
     */
    public Calendar getMostRecentlyActive() {
        return mostRecentlyActive;
    }

    public void setMostRecentlyActive(Calendar mostRecentlyActive) {
        this.mostRecentlyActive = mostRecentlyActive;
    }

    /**
     * @return A disabled AccessToken will never allow access to authenticated
     *         services
     */
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The number of seconds of permitted inactivity before this
     *         AccessToken will expire. An expired AccessToken will never allow
     *         access to authenticated services
     */
    public Integer getAllowableInactiveSeconds() {
        return allowableInactiveSeconds;
    }

    public void setAllowableInactiveSeconds(Integer allowableInactiveSeconds) {
        this.allowableInactiveSeconds = allowableInactiveSeconds;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void initializeFrom(AccessTokenDto dto) {

        BeanUtils.copyProperties(dto, this);

        if (dto.getUserId() != null) {
            User u = getContext().getLocator().findById(User.class, dto.getUserId());

            if (u == null) {
                throw new BadRequestException(
                        String.format(
                                "Access token references user that does not exist. Userid='%s'",
                                dto.getUserId()));
            } else {
                this.setUser(u);
            }

        }
    }

    @Override
    public AccessTokenDto toDto() {
        AccessTokenDto d = new AccessTokenDto();

        BeanUtils.copyProperties(this, d);

        if (this.user != null)
            d.setUserId(this.user.getId());

        if (this.user.getUserGroups() != null) {
            d.setGroups(new LinkedList<String>());
            for (UserGroup r : this.user.getUserGroups())
                d.getGroups().add(r.getName());
        }

        return d;
    }
    
    /**
     * Checks the validity of the token (ie, not expired, not disabled) and, if
     * valid, updates the token to reflect that an activity was attempted. This
     * will extend the life of the token. This operation will be performed in a
     * brand new transaction scope independent of any existing transaction
     * scope, guaranteeing that the token will be updated regardless of the
     * success/failure of any other operations.
     * <p>
     * The general policy is that any attempt to perform an operation using a
     * token should result in a 'touch'.
     * </p>
     * 
     * @throws AccessUnauthorizedException
     *             If the token is not valid as determined by
     *             {@link AccessToken#isValid()}
     */
    public void touch() throws AccessUnauthorizedException {

        logger.trace("Updating token's MostRecentlyActive value. token='{}'",
                getId());

        if (!isValid())
            throw new AccessUnauthorizedException("AccessToken is not valid.  It may have expired or been disabled.");

        this.setMostRecentlyActive(Calendar.getInstance());

        final AccessToken t = this;
        final EntityManager e = getContextProvider().getContext().getEm();
        
        getContext().getTxnAccss().requiresNew(new TransactionWrappedLogic() {
            public void execute() {

                e.merge(t);

            }
        });

    }

    /**
     * Checks the validity of this token.
     */
    public boolean isValid() {
        // A valid token has been used sooner than its allowableInactiveSeconds
        // and has not been disabled.
        Calendar now = Calendar.getInstance();

        if (!enabled)
            return false;

        if (allowableInactiveSeconds < 0)
            return true;
        else if (now.getTimeInMillis() - mostRecentlyActive.getTimeInMillis() > allowableInactiveSeconds * 1000)
            return false;
        else
            return true;

    }

    /**
     * Generates and assigns a UUID for this access token
     */
    @PrePersist
    public void selfAssignUUID() {
        if (StringUtils.isEmpty(this.getId()))
            this.setId(UUID.randomUUID().toString());
    }
    
    /*
     * logs out the user by 
     * disabling the token id
     * 
     */
    public void disable()
    {       
        this.setEnabled(false);
        getContext().getEm().persist(this);

    }
}
