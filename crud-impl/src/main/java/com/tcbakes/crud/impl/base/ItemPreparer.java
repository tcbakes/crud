package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.spec.base.Dto;

public interface ItemPreparer<C extends Crud<C,D,I>, D extends Dto<I>, I> {

	void prepare(C item);
}
