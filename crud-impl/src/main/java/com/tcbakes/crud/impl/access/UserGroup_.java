package com.tcbakes.crud.impl.access;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UserGroup.class)
public abstract class UserGroup_ {

	public static volatile SingularAttribute<UserGroup, Long> id;
	public static volatile SingularAttribute<UserGroup, String> name;
	public static volatile SingularAttribute<UserGroup, String> operationStr;
	public static volatile ListAttribute<UserGroup, ACL> acls;
	public static volatile SingularAttribute<UserGroup, ACL> acl;

}

