package com.tcbakes.crud.impl.base;

/**
 * A series of methods that execute in a very specific transactional scope.
 * These methods will delegate the execution of the logic to a method provided
 * by the client.
 * 
 * <p>
 * A typical usage pattern might look something like this
 * 
 * <pre>
 * 
 * final CrudItem t = someItem;
 * final EntityManager e = entityManager;
 * 
 * // update the CrudItem in a new transaction, guaranteeing that the update will
 * // occur regardless of the success or failure of the outer transaction.
 * txnAccess.requiresNew(new TransactionWrappedLogic() {
 * 	public void execute() {
 * 
 * 		e.merge(t);
 * 
 * 	}
 * });
 * </pre>
 * 
 * @author Tristan Baker
 * 
 */
public interface TransactionAccessLocal {

	/**
	 * Starts a new transaction.
	 * 
	 * @param logic
	 *            logic to execute in the new transaction
	 */
	public void requiresNew(TransactionWrappedLogic logic);

	/**
	 * Starts a new transaction if one doesn't exist. Joins an existing one if
	 * it does
	 * 
	 * @param logic
	 *            logic to execute in a mandatory transaction
	 */
	public void mandotory(TransactionWrappedLogic logic);

	/**
	 * Executes outside the scope of an existing transaction, if one already
	 * exists
	 * 
	 * @param logic
	 *            logic to never execute in a transaction
	 */
	public void never(TransactionWrappedLogic logic);

	/**
	 * Raises an error if the excution takes place within an existing
	 * transaction
	 * 
	 * @param logic
	 *            logic that is not supported within the scope of a transaction
	 */
	public void notSupported(TransactionWrappedLogic logic);

	/**
	 * @param logic
	 */
	public void required(TransactionWrappedLogic logic);

}
