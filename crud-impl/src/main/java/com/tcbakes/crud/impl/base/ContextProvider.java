package com.tcbakes.crud.impl.base;

/**
 * A Provider of an ExecutionContext.
 * 
 * @author Tristan Baker
 * 
 */
public interface ContextProvider {

	/**
	 * @return The current {@link ExecutionContext}
	 */
	public ExecutionContext getContext();

	/**
	 * @param c
	 *            The an {@link ExecutionContext} as the current one
	 */
	public void set(ExecutionContext c);

	/**
	 * Unset the current {@link ExecutionContext}, such that subsequent calls to
	 * {@link #getContext()} will return null, until the next call to
	 * {@link #set(ExecutionContext)} with a non-null ExecutionContext
	 */
	public void unset();
}
