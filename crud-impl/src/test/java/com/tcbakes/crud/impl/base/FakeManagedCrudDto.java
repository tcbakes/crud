package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.spec.base.ManagedDto;


public class FakeManagedCrudDto extends ManagedDto<Long> {

	private static final long serialVersionUID = 1L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;
	
}
