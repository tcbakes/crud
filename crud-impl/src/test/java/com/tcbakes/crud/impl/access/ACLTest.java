package com.tcbakes.crud.impl.access;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.SingularAttribute;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tcbakes.crud.impl.base.ContextProvider;
import com.tcbakes.crud.impl.base.EntityFactory;
import com.tcbakes.crud.impl.base.ExecutionContext;
import com.tcbakes.crud.impl.base.Locator;
import com.tcbakes.crud.impl.base.TransactionAccessLocal;
import com.tcbakes.crud.spec.access.ACLDto;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;

@SuppressWarnings({ "unchecked", "serial" })
public class ACLTest {

    @InjectMocks
    ACL acl;
    
    @Mock
    User mockActor;
	@Mock 
	ContextProvider mockContextProvider;
    @Mock
    Locator mockLocator;
    @Mock
    EntityManager mockEm;
    @Mock
    EntityFactory mockEf;
    @Mock
    TransactionAccessLocal mockTxnAccess;
    
    
    ExecutionContext executionContext;
    
    private static final String CC_GROUP = "CCROUTINGRPMANAGERS";
    private static final String USER_GROUP = "USER";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        
		executionContext = new ExecutionContext();
		
		when(mockContextProvider.getContext()).thenReturn(executionContext);
		
		executionContext.setLocator(mockLocator);
		executionContext.setEm(mockEm);
		executionContext.setTxnAccss(mockTxnAccess);
		executionContext.setItemFactory(mockEf);
		executionContext.setActor(mockActor);
        
        when(mockActor.getId()).thenReturn(1L);
        
        UserGroup fakeUg = new UserGroup();
        fakeUg.setName(CC_GROUP);

        fakeUg.setAcls(new ArrayList<ACL>());
        fakeUg.getAcls().add(acl);
        
        acl.setGroups(new ArrayList<UserGroup>());
        acl.getGroups().add(fakeUg);
        
        acl.setId(1L);
        acl.setOwningUser(mockActor);
    }

    @Test
    public void initializeFromTest() {
        
        ACLDto dto = new ACLDto();

        dto.setId(1L);
        dto.setOwningUserId(1L);
        dto.setUserGroups(new ArrayList<String>() {
            {
                add(CC_GROUP);
            }
        });

        UserGroup mockUg = mock(UserGroup.class);

        when(
                mockLocator.findSingleByFieldValue(any(Class.class),
                        any(SingularAttribute.class), anyString())).thenReturn(
                mockUg);

        acl.initializeFrom(dto);

        Assert.assertNotNull(acl.getGroups());
        Assert.assertTrue(acl.getGroups().size() > 0);
    }

    @Test
    public void toDtoTest() {      
        ACLDto dto = acl.toDto();

        Assert.assertNotNull(dto);
        Assert.assertTrue(dto.getOwningUserId() != null);
        Assert.assertTrue(dto.getUserGroups().size() > 0);
    }
    
    @Test
    public void update_Test(){
        ACL fakeAcl = new ACL();
        fakeAcl.setId(1L);
        fakeAcl.setOwningUser(mockActor);
        fakeAcl.setGroups(new ArrayList<UserGroup>());
        
        acl.update(fakeAcl);
    }
    
    @Test
    public void doUpdateLogic_success() {
        ACL fakeAcl = new ACL();
        fakeAcl.setId(1L);
        fakeAcl.setOwningUser(mockActor);
        fakeAcl.setGroups(new ArrayList<UserGroup>());
        
        acl.doUpdateLogic(fakeAcl);
    }
    
    @Test(expected=AccessForbiddenException.class)
    public void doUpdateLogic_nonOwningUser() {
        User unauthorizedUser = mock(User.class);
        when(unauthorizedUser.getId()).thenReturn(2L);
        executionContext.setActor(unauthorizedUser);
        
        ACL fakeAcl = new ACL();
        fakeAcl.setId(1L);
        fakeAcl.setOwningUser(mockActor);
        fakeAcl.setGroups(new ArrayList<UserGroup>());
        
        acl.doUpdateLogic(fakeAcl);
    }
    
    @Test(expected=AccessForbiddenException.class)
    public void doUpdateLogic_notOwnerAndNotInUsergroup() {       
        when(mockActor.getUserGroups()).thenReturn(new ArrayList<UserGroup>());
        
        User unauthorizedUser = mock(User.class);
        when(unauthorizedUser.getId()).thenReturn(2L);
        executionContext.setActor(unauthorizedUser);
        
        ACL fakeAcl = new ACL();
        fakeAcl.setId(1L);
        fakeAcl.setOwningUser(mockActor);
        fakeAcl.setGroups(new ArrayList<UserGroup>());
        
        UserGroup fakeUg = new UserGroup();
        fakeUg.setName(USER_GROUP);
        
        fakeAcl.getGroups().add(fakeUg);
        
        acl.doUpdateLogic(fakeAcl);
    }
    
    @Test
    public void read_Test() {
        acl.read();
    }
    
    @Test
    public void doReadLogic_success() {
        acl.doReadLogic();
    }

    @Test(expected=AccessForbiddenException.class)
    public void doReadLogic_nonOwningUser() {
        User unauthorizedUser = mock(User.class);
        when(unauthorizedUser.getId()).thenReturn(2L);
        
        executionContext.setActor(unauthorizedUser);
        
        acl.doReadLogic();
    }
    
    @Test(expected=AccessForbiddenException.class)
    public void doReadLogic_nonOwningAndNotInUsergroup() {
        User unauthorizedUser = mock(User.class);
        when(unauthorizedUser.getId()).thenReturn(2L);
        
        UserGroup fakeUg = new UserGroup();
        fakeUg.setName(USER_GROUP);
        
        acl.setGroups(new ArrayList<UserGroup>());
        acl.getGroups().add(fakeUg);
        
        
        executionContext.setActor(unauthorizedUser);
        
        
        acl.doReadLogic();
    }
    
    @Test()
    public void operationsPermitted_Test(){
    	User u = new FakeUser();
    	u.setUserGroups(new ArrayList<UserGroup>());
    	u.getUserGroups().add(new UserGroup());
    	u.getUserGroups().get(0).setName("TestGroup");
    	
    	acl.setGroups(new ArrayList<UserGroup>());
    	acl.getGroups().add(new UserGroup());
    	acl.getGroups().get(0).setName("TestGroup");
    	acl.getGroups().get(0).setOperations(new ArrayList<String>());
    	acl.getGroups().get(0).getOperations().add("READ");
    	acl.getGroups().get(0).getOperations().add("UPDATE");
    	
    	List<String> permittedOperations = acl.operationsPermitted(u);
    	
    	Assert.assertNotNull(permittedOperations);
    	Assert.assertTrue(permittedOperations.contains("READ"));
    	Assert.assertTrue(permittedOperations.contains("UPDATE"));
    	
    	
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void operationsPermitted_NullArg_Test(){
    	acl.operationsPermitted(null);
    }
    
    /**
     * The owning user is allowed to perform all operations
     */
    @Test()
    public void operationsPermitted_ALL_Test(){
    	User u = new FakeUser();
    	u.setId(100L);
    	u.setUserGroups(new ArrayList<UserGroup>());
    	u.getUserGroups().add(new UserGroup());
    	u.getUserGroups().get(0).setName("TestGroup");
    	
    	acl.setOwningUser(new FakeUser());
    	acl.getOwningUser().setId(100L);
    	

    	List<String> permittedOperations = acl.operationsPermitted(u);
    	
    	Assert.assertNotNull(permittedOperations);
    	Assert.assertTrue(permittedOperations.contains("ALL"));
    }
    
    /**
     * Test that an individual operation is permitted when 
     * the operation appears in the appropriate usergroup
     */
    @Test()
    public void operationPermitted_Test(){
    	User u = new FakeUser();
    	u.setId(100L);
    	u.setUserGroups(new ArrayList<UserGroup>());
    	u.getUserGroups().add(new UserGroup());
    	u.getUserGroups().get(0).setName("TestGroup");
    	
    	acl.setGroups(new ArrayList<UserGroup>());
    	acl.getGroups().add(new UserGroup());
    	acl.getGroups().get(0).setName("TestGroup");
    	acl.getGroups().get(0).setOperations(new ArrayList<String>());
    	acl.getGroups().get(0).getOperations().add("READ");
    	

    	boolean isPermitted = acl.operationPermitted(u, "READ");
    	
    	Assert.assertTrue(isPermitted);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void operationPermitted_NullActor_Test(){
  	
    	acl.operationPermitted(null, "READ");
    	
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void operationPermitted_EmptyOp_Test(){
  	
    	acl.operationPermitted(new FakeUser(), null);
    	
    }
    
}
