package com.tcbakes.crud.impl.access;

import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.LinkedList;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.BeanUtils;

import com.tcbakes.crud.impl.base.ContextProvider;
import com.tcbakes.crud.impl.base.ExecutionContext;
import com.tcbakes.crud.impl.base.Locator;
import com.tcbakes.crud.impl.base.TransactionAccessLocal;
import com.tcbakes.crud.spec.access.AccessTokenDto;
import com.tcbakes.crud.spec.exception.AccessUnauthorizedException;
import com.tcbakes.crud.spec.exception.BadRequestException;;

public class AccessTokenTest {

	@InjectMocks AccessToken token;

	@Mock ContextProvider mockContextProvider;
	@Mock Locator mockLocator;
	@Mock EntityManager mockEm;
	@Mock TransactionAccessLocal access;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
	
		ExecutionContext ctx = new ExecutionContext();
		
		when(mockContextProvider.getContext()).thenReturn(ctx);
		
		ctx.setLocator(mockLocator);
		ctx.setEm(mockEm);
		ctx.setTxnAccss(access);
		
		
    	token.setId("7f7c09b5-68a8-4562-8395-7dc3c9e7538e");
    	token.setAllowableInactiveSeconds(1200);
    	token.setEnabled(true);
    	token.setMostRecentlyActive(Calendar.getInstance());
    	token.setStart(Calendar.getInstance());
    	token.setUser(new FakeUser());
		token.getUser().setId(new Long(123));
		token.getUser().setUserGroups(new LinkedList<UserGroup>());
		token.getUser().getUserGroups().add(new UserGroup());
		token.getUser().getUserGroups().get(0).setId(new Long(1234));
		token.getUser().getUserGroups().get(0).setName("bar");
		
	}
	
	@Test(expected=BadRequestException.class)
	public void fromDtoTest_BadRequest(){
		AccessTokenDto dto = new AccessTokenDto();
		dto.setId("foo");
		dto.setEnabled(false);
		dto.setGroups(new LinkedList<String>());
		dto.getGroups().add("BAR");
		dto.setMostRecentlyActive(Calendar.getInstance());
		dto.setUserId(new Long(123));
		
		when(mockLocator.findById(User.class, dto.getUserId())).thenReturn(null);
		
		token.initializeFrom(dto);
	}
	
	@Test
	public void fromDtoTest(){
		AccessTokenDto dto = new AccessTokenDto();
		dto.setId("foo");
		dto.setEnabled(false);
		dto.setGroups(new LinkedList<String>());
		dto.getGroups().add("BAR");
		dto.setMostRecentlyActive(Calendar.getInstance());
		dto.setUserId(new Long(123));
		
		when(mockLocator.findById(User.class, dto.getUserId())).thenReturn(new FakeUser());
		
		token.initializeFrom(dto);
		
		Assert.assertEquals(dto.getId(), token.getId());
		Assert.assertEquals(dto.getEnabled(), token.getEnabled());
		Assert.assertNotNull(token.getUser());
		Assert.assertEquals(dto.getMostRecentlyActive().getTimeInMillis(), token.getMostRecentlyActive().getTimeInMillis());
	}
	
	@Test
	public void toDtoTest(){
		
		AccessTokenDto dto = token.toDto();
		
		Assert.assertEquals(token.getId(), dto.getId());
		Assert.assertEquals(token.getEnabled(), dto.getEnabled());
		Assert.assertNotNull(dto.getUserId());
		Assert.assertEquals(token.getMostRecentlyActive().getTimeInMillis(), dto.getMostRecentlyActive().getTimeInMillis());
		Assert.assertEquals(token.getUser().getUserGroups().get(0).getName(), dto.getGroups().get(0));
	}
	
	@Test
	public void invalidToken_Disabled() {
		AccessToken newState = new AccessToken();
		BeanUtils.copyProperties(token, newState);
		
		newState.setEnabled(false);
		
		
		Assert.assertFalse(newState.isValid());
	}
	
	@Test
	public void alwaysVaildToken() {
		AccessToken newState = new AccessToken();
		BeanUtils.copyProperties(token, newState);
		
		newState.setAllowableInactiveSeconds(-1);
		
		
		Assert.assertTrue(newState.isValid());
	}
	
	@Test
	public void invalidToken_expired() {
		AccessToken newState = new AccessToken();
		BeanUtils.copyProperties(token, newState);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1950);
		
		newState.setMostRecentlyActive(cal);
		
		
		Assert.assertFalse(newState.isValid());
	}
	
	@Test(expected=AccessUnauthorizedException.class)
	public void touchTest() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1950);
		
		token.setMostRecentlyActive(cal);
		
		token.touch();
	}
	
	@Test
	public void touchTest_Unauthorized() {
		
		token.touch();
	}
	
	@Test
	public void selfAssignUUID(){
		AccessToken token = new AccessToken();
		token.selfAssignUUID();
		Assert.assertNotNull(token.getId());
	}
	
	@Test
	public void disableAccessTokenTest()
	{
	    token.disable();
	    Assert.assertFalse(token.getEnabled());

	}
}
