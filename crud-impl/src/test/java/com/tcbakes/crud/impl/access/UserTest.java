package com.tcbakes.crud.impl.access;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.BeanUtils;

import com.tcbakes.crud.impl.base.ContextProvider;
import com.tcbakes.crud.impl.base.EntityFactory;
import com.tcbakes.crud.impl.base.ExecutionContext;
import com.tcbakes.crud.impl.base.Locator;
import com.tcbakes.crud.impl.base.TransactionAccessLocal;
import com.tcbakes.crud.tools.CrudItemsDirectory;

public class UserTest {

	@InjectMocks FakeUser user;

	@Mock Locator mockLocator;
	@Mock EntityManager mockEm;
	@Mock TransactionAccessLocal mockTxnAccess;
	@Mock EntityFactory mockEf;
	@Mock Query mockQuery;
	@Mock ContextProvider mockContextProvider;
	@Mock CrudItemsDirectory mockDirectory;
	
	FakeUser newUserState;
	
	User mockActor;
	
	ExecutionContext executionContext;
	
	@Before
	public void setup() throws IllegalAccessException, InvocationTargetException{
		MockitoAnnotations.initMocks(this);
		
		mockActor = Mockito.mock(User.class);
		Mockito.when(mockActor.createOperationPermitted(Mockito.anyString())).thenReturn(true);
		
		executionContext = new ExecutionContext();
		
		when(mockContextProvider.getContext()).thenReturn(executionContext);
		
		executionContext.setLocator(mockLocator);
		executionContext.setEm(mockEm);
		executionContext.setTxnAccss(mockTxnAccess);
		executionContext.setItemFactory(mockEf);
		executionContext.setActor(mockActor);
		
        when(mockEf.getDirectory()).thenReturn(mockDirectory);
        when(mockDirectory.getOperationsByClass(FakeUser.class))
                .thenReturn(new HashMap<String, String>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("CREATE", "FAKEUSER");
                    }
                });
        
		ACL mockACL = Mockito.mock(ACL.class);
    	when(mockACL.operationPermitted(any(User.class), any(String.class))).thenReturn(true);

    	//Create a valid mockUser. Tests can
		//set individual fields invalid for negative testing
		user.setId(new Long(123));
    	user.setAcl(mockACL);
    	user.setUserGroups(new LinkedList<UserGroup>());
    	user.getUserGroups().add(new UserGroup());
    	user.getUserGroups().get(0).setName("TestGroup123");
    	

		    	
    	newUserState = new FakeUser();
    	BeanUtils.copyProperties(user, newUserState);
    	
    	
	}
	
    @Test
    public void fromDtoTest()
    {
        FakeUserDto dto = new FakeUserDto();
        dto.setUserGroups(new LinkedList<String>());
        dto.getUserGroups().add("ROLE_USER");
        
        when(mockLocator.findSingleByFieldValue(UserGroup.class, UserGroup_.name, "ROLE_USER")).thenReturn(new UserGroup());
        
        user.initializeFrom(dto);
    	
    	Assert.assertEquals(dto.getUserGroups().size(), user.getUserGroups().size());
    	
    }
    
    
    @Test
    public void toDtoTest()
    {
        FakeUserDto dto = (FakeUserDto) user.toDto();
    	
    	Assert.assertEquals(user.getId(), dto.getId());
    	Assert.assertEquals(user.getUserGroups().size(), dto.getUserGroups().size());
    	
    }
    
    @Test
    public void createUserTest(){
    	
    	//Create
    	user.setId(null);
    	user.create();
    	
    	Assert.assertNotNull(user.getAcl());
    }
    
    @Test
	public void createUserTest_noRoles() {
    	//Allow creation of users without any roles.
    	user.setId(null);
    	user.setUserGroups(new LinkedList<UserGroup>());
    		
    	user.create();
    }
    
    @Test
    public void deleteUserTest(){

        List<ACL> acls = new ArrayList<ACL>();
        
        when(mockEm.createNamedQuery("ACL.findAllAclsForUser")).thenReturn(mockQuery);
        when(mockQuery.setParameter(any(String.class), any(Object.class))).thenReturn(mockQuery);
        when(mockQuery.getResultList()).thenReturn(acls);

    	user.delete();
    }
    
	@Test
    public void updateUserTest(){
    	
    	//Change the email address
		UserGroup r = new UserGroup();
		r.setId(new Long(123));
		r.setName("FOO");
		
		newUserState.getUserGroups().add(r);
		
    	user.update(newUserState);
    }
	
	@Test
	public void getCreateOperations_Test(){
		User u = new FakeUser();
		u.setUserGroups(new LinkedList<UserGroup>());
		u.getUserGroups().add(new UserGroup());
		u.getUserGroups().get(0).setName("TestGroup1");
		u.getUserGroups().get(0).setOperations(new LinkedList<String>());
		u.getUserGroups().get(0).getOperations().add("CREATE_FOO");
		u.getUserGroups().get(0).getOperations().add("READ");
		
		u.getUserGroups().add(new UserGroup());
		u.getUserGroups().get(1).setName("TestGroup2");
		u.getUserGroups().get(1).setOperations(new LinkedList<String>());
		u.getUserGroups().get(1).getOperations().add("CREATE_BAR");
		u.getUserGroups().get(1).getOperations().add("CREATE_FOO");
		u.getUserGroups().get(1).getOperations().add("UPDATE");
		
		List<String> createOps = u.getCreateOperations();
		
		Assert.assertEquals(2, createOps.size());
		Assert.assertTrue(createOps.contains("CREATE_FOO"));
		Assert.assertTrue(createOps.contains("CREATE_BAR"));
	}
	
	@Test
	public void createOperationPermitted_OpPermitted_Test(){
		User u = new FakeUser();
		u.setUserGroups(new LinkedList<UserGroup>());
		u.getUserGroups().add(new UserGroup());
		u.getUserGroups().get(0).setName("TestGroup1");
		u.getUserGroups().get(0).setOperations(new LinkedList<String>());
		u.getUserGroups().get(0).getOperations().add("CREATE_FOO");
		u.getUserGroups().get(0).getOperations().add("READ");
		
		u.getUserGroups().add(new UserGroup());
		u.getUserGroups().get(1).setName("TestGroup2");
		u.getUserGroups().get(1).setOperations(new LinkedList<String>());
		u.getUserGroups().get(1).getOperations().add("CREATE_BAR");
		u.getUserGroups().get(1).getOperations().add("CREATE_FOO");
		u.getUserGroups().get(1).getOperations().add("UPDATE");
		
		Assert.assertTrue(u.createOperationPermitted("CREATE_FOO"));

		
		
	}
	
	@Test
	public void createOperationPermitted_OpNotPermitted_Test(){
		User u = new FakeUser();
		u.setUserGroups(new LinkedList<UserGroup>());
		u.getUserGroups().add(new UserGroup());
		u.getUserGroups().get(0).setName("TestGroup1");
		u.getUserGroups().get(0).setOperations(new LinkedList<String>());
		u.getUserGroups().get(0).getOperations().add("CREATE_FOO");
		u.getUserGroups().get(0).getOperations().add("READ");
		
		u.getUserGroups().add(new UserGroup());
		u.getUserGroups().get(1).setName("TestGroup2");
		u.getUserGroups().get(1).setOperations(new LinkedList<String>());
		u.getUserGroups().get(1).getOperations().add("CREATE_BAR");
		u.getUserGroups().get(1).getOperations().add("CREATE_FOO");
		u.getUserGroups().get(1).getOperations().add("UPDATE");
		
		Assert.assertFalse(u.createOperationPermitted("CREATE_XYZ"));
		
	}
}
