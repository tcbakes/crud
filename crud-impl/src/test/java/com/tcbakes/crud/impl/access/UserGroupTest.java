package com.tcbakes.crud.impl.access;

import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.BeanUtils;

import com.tcbakes.crud.impl.base.ContextProvider;
import com.tcbakes.crud.impl.base.EntityFactory;
import com.tcbakes.crud.impl.base.ExecutionContext;
import com.tcbakes.crud.impl.base.Locator;
import com.tcbakes.crud.impl.base.TransactionAccessLocal;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.spec.access.UserGroupDetailsDto;
import com.tcbakes.crud.spec.access.UserGroupDto;
import com.tcbakes.crud.tools.CrudItemsDirectory;

public class UserGroupTest {

	@InjectMocks UserGroup group;

    @Mock
    Locator mockLocator;
    @Mock
    EntityManager mockEm;
    @Mock
    EntityFactory mockEf;
    @Mock
    TransactionAccessLocal mockTxnAccess;
	@Mock 
	ContextProvider mockContextProvider;
	@Mock 
	CrudItemsDirectory mockDirectory;
    
	User mockActor;
    
	UserGroup newGroupState;
	
	ExecutionContext executionContext;
	 
	@Before
	public void setup() throws IllegalAccessException, InvocationTargetException{
		MockitoAnnotations.initMocks(this);
		
		mockActor = Mockito.mock(User.class);
		Mockito.when(mockActor.createOperationPermitted(Mockito.anyString())).thenReturn(true);
		
		ACL mockACL = Mockito.mock(ACL.class);
		Mockito.when(mockACL.operationPermitted(Mockito.any(User.class), Mockito.anyString())).thenReturn(true);
		
		executionContext = new ExecutionContext();
		
		when(mockContextProvider.getContext()).thenReturn(executionContext);
		
		executionContext.setLocator(mockLocator);
		executionContext.setEm(mockEm);
		executionContext.setTxnAccss(mockTxnAccess);
		executionContext.setItemFactory(mockEf);
		executionContext.setActor(mockActor);

        when(mockEf.getDirectory()).thenReturn(mockDirectory);
        when(mockDirectory.getOperationsByClass(UserGroup.class))
                .thenReturn(new HashMap<String, String>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("CREATE", "CREATE_FAKEUSER");
                    }
                });
        when(mockDirectory.getValidOperations()).thenReturn(new HashSet<String>() {
            private static final long serialVersionUID = 1L;
            {
                add("CREATE_FAKEUSER");
            }
        });

		group.setId(new Long(123));
		group.setName("foo");
		
    	newGroupState = new UserGroup();
    	BeanUtils.copyProperties(group, newGroupState);
		
        group.setAcl(mockACL);

	}

	@Test
	public void toDto_NullOperations_Test() {
		
		UserGroupDto d = group.toDto();
		
		Assert.assertEquals(group.getId(), d.getId());
		Assert.assertEquals(group.getName(), d.getName());
	}
	
	@Test
	public void toDto_Test() {
		UserGroup g = new UserGroup();
		g.setId(100L);
		g.setName("Hello");
		g.setOperations(new LinkedList<String>());
		g.getOperations().add("TESTOP");
		
		UserGroupDto d = g.toDto();
		
		Assert.assertEquals(g.getId(), d.getId());
		Assert.assertEquals(g.getName(), d.getName());
		Assert.assertEquals(g.getOperations().size(), d.getOperations().size());
	}
	
	
	@Test
	public void fromDtoTest() {
		
		UserGroupDto d = new UserGroupDto();
		d.setId(new Long(123));
		d.setName("foo");
		d.setOperations(new LinkedList<String>());
		d.getOperations().add("TESTOP");
		
		UserGroup g = new UserGroup();
		g.initializeFrom(d);
		
		Assert.assertEquals(d.getId(), g.getId());
		Assert.assertEquals(d.getName(), g.getName());
		Assert.assertEquals(d.getOperations().size(), g.getOperations().size());
	}
	
    @Test
    public void createGroupTest() {	  
        group.setOperations(new LinkedList<String>());
        group.getOperations().add("CREATE_FAKEUSER");
        
        group.create();
    }
	
	@Test(expected=CrudItemValidationException.class)
	public void createGroupTest_nullName(){
		group.setName(null);
		group.create();
	}
	
	@Test(expected=CrudItemValidationException.class)
	public void createGroupTest_nullOperation() {
	    group.setOperations(null);
	    group.create();
	}
    
	@Test(expected=CrudItemValidationException.class)
	public void createGroupTest_nameCollides(){
		
		UserGroup collidingGroup = new UserGroup();
		when(mockLocator.findSingleByFieldValue(UserGroup.class, UserGroup_.name, "foo")).thenReturn(collidingGroup);
		
		group.create();
	}
	
    @Test
    public void updateGroupTest() {
    
    	newGroupState.setName("baz");
    	newGroupState.setOperations(new LinkedList<String>());
    	newGroupState.getOperations().add("CREATE_FAKEUSER");
        
    	when(mockLocator.findSingleByFieldValue(UserGroup.class, UserGroup_.name, newGroupState.getName())).thenReturn(null);
    	
    	group.update(newGroupState);
    	
    }

    @Test(expected = CrudItemValidationException.class)
    public void updateGroup_nullName() {

        newGroupState.setName(null);

        group.update(newGroupState);

    }

    @Test(expected = CrudItemValidationException.class)
    public void updateGroup_nameCollides() {
    
    	UserGroup collidingGroup = new UserGroup();
    	collidingGroup.setId(new Long(456));

    	newGroupState.setName("baz");
    	
    	when(mockLocator.findSingleByFieldValue(UserGroup.class, UserGroup_.name, newGroupState.getName())).thenReturn(collidingGroup);
    	
    	group.update(newGroupState);
    }

    @Test
    public void deleteGroupTest() {
        group.delete();
    }

    @Test
    public void convertToString_TwoOps_Test(){
    	UserGroup g = new UserGroup();
    	g.setOperations(new LinkedList<String>());
    	g.getOperations().add("FOO");
    	g.getOperations().add("BAR");
    	String opString = g.convertToString();
    	
    	Assert.assertTrue(opString.contains("FOO"));
    	Assert.assertTrue(opString.contains("BAR"));
    	Assert.assertTrue(opString.contains(","));
    }
    
    @Test
    public void convertToString_ZeroOps_Test(){
    	UserGroup g = new UserGroup();
    	g.setOperations(new LinkedList<String>());
    	String opString = g.convertToString();
    	
    	Assert.assertEquals("", opString);
    }
    
    @Test
    public void buildFromString_TwoOps_est(){
    	UserGroup g = new UserGroup();
    	g.buildFromString("FOO,BAR");
    	
    	Assert.assertEquals(2, g.getOperations().size());
    }
    
    @Test
    public void buildFromString_OneOp_Test(){
    	UserGroup g = new UserGroup();
    	g.buildFromString("FOO");
    	
    	Assert.assertEquals(1, g.getOperations().size());
    }
    
    @Test
    public void buildFromString_ZeroOps_Test(){
    	UserGroup g = new UserGroup();
    	g.buildFromString("");
    	
    	Assert.assertEquals(null, g.getOperations());
    }
    
    @Test
    public void toDetailsDto_Test(){
    	
    	List<User> queryResult = new LinkedList<User>();
    	User fakeUser = new FakeUser();
    	fakeUser.setId(100L);
    	queryResult.add(fakeUser);
    	
    	Query mockQuery = Mockito.mock(Query.class);
    	Mockito.when(mockEm.createNamedQuery(Mockito.anyString())).thenReturn(mockQuery);
    	Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.anyObject())).thenReturn(mockQuery);
    	Mockito.when(mockQuery.getResultList()).thenReturn(queryResult);
    	
    	UserGroupDetailsDto details = group.toDetailsDto();
    	
    	Assert.assertEquals(group.getName(), details.getName());
    	Assert.assertTrue(details.getMembers().containsKey(fakeUser.getFriendlyUsername()));
    }
    
    @Test(expected=CrudItemValidationException.class)
    public void validationLogicInvalidOperation_Test() {
        List<String> array = new ArrayList<String>();
        array.add("TEST");
        group.setOperations(array);

        group.doCreateLogic();
    }
}
