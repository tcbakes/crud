package com.tcbakes.crud.impl.base;

public class PretendCrudDto extends AbstractPretendCrudDto {

	private static final long serialVersionUID = 1L;
	
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

}
