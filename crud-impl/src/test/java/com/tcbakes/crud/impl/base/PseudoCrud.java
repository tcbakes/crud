package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.tools.ProvidesBackingForDto;


@ProvidesBackingForDto(PseudoCrudDto.class)
public class PseudoCrud extends Crud<PseudoCrud, PseudoCrudDto, Long> {

	private static final long serialVersionUID = 1L;
	
	Long id;
	String something;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {

		this.id = id;
	}

	public String getSomething() {
		return something;
	}

	public void setSomething(String something) {
		this.something = something;
	}

	@Override
	public void initializeFrom(PseudoCrudDto dto) {
		this.id = dto.getId();
		this.something = dto.getSomething();
	}

	@Override
	public PseudoCrudDto toDto() {
		PseudoCrudDto d = new PseudoCrudDto();
		d.setId(this.id);
		d.setSomething(this.something);
		return d;
	}

}
