package com.tcbakes.crud.impl.base;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.paging.PageOfItemsDto;

public class PageOfItemsTest {

	@InjectMocks
	PageOfItems<FakeManagedCrud, FakeManagedCrudDto, Long> page;

	@Mock
	EntityFactory mockFactory;

	@Mock
	Locator mockLocator;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		page.setManagedClass(FakeManagedCrud.class);

	}

	@Test
	public void read_RecordsEvenlyDivisibleByPageNumber() {

		List<FakeManagedCrud> list = new LinkedList<FakeManagedCrud>();
		for (int x = 0; x < 10; x++) {
			FakeManagedCrud item = mock(FakeManagedCrud.class);
			list.add(item);
		}

		when(mockFactory.findAllAndBuild(1, 5, FakeManagedCrud.class, null)).thenReturn(
				list.subList(0, 5));
		when(mockLocator.count(FakeManagedCrud.class)).thenReturn(10);

		page.read(1, 5);

		Assert.assertEquals(5, page.getPageItems().size());
		Assert.assertEquals(5, page.getNumberOfItemsThisPage());
		Assert.assertEquals(1, page.getThisPageNumber());
		Assert.assertEquals(2, page.getTotalNumberOfPages());

	}

	/**
	 * Tests that when the record count is not evenly divisible by page number,
	 * an extra page is added to the total number of pages
	 */
	@Test
	public void read_RecordsNotEvenlyDivisibleByPageNumber() {

		List<FakeManagedCrud> list = new LinkedList<FakeManagedCrud>();
		for (int x = 0; x < 10; x++) {
			FakeManagedCrud item = mock(FakeManagedCrud.class);
			list.add(item);
		}

		when(mockFactory.findAllAndBuild(1, 4, FakeManagedCrud.class, null)).thenReturn(
				list.subList(0, 4));
		when(mockLocator.count(FakeManagedCrud.class)).thenReturn(10);

		page.read(1, 4);

		Assert.assertEquals(4, page.getPageItems().size());
		Assert.assertEquals(4, page.getNumberOfItemsThisPage());
		Assert.assertEquals(1, page.getThisPageNumber());
		Assert.assertEquals(3, page.getTotalNumberOfPages());

	}

	@Test
	public void read_ItemsRemoved() {

		List<FakeManagedCrud> list = new LinkedList<FakeManagedCrud>();

		for (int x = 0; x < 10; x++) {
			FakeManagedCrud item = mock(FakeManagedCrud.class);
			if (x % 2 == 0)
				doThrow(new AccessForbiddenException()).when(item).doReadLogic();

			list.add(item);
		}

		when(mockFactory.findAllAndBuild(anyInt(), anyInt(), any(Class.class), any(ItemPreparer.class)))
				.thenReturn(list);

		page.read(1, 10);

		Assert.assertEquals(5, page.getNumberOfItemsThisPage());
		Assert.assertEquals(5, page.getPageItems().size());

	}

	@Test
	public void toDtoTest() {

		page.setPageItems(new LinkedList<FakeManagedCrud>());
		for (int x = 0; x < 2; x++) {
			FakeManagedCrud item = mock(FakeManagedCrud.class);
			when(item.toDto()).thenReturn(new FakeManagedCrudDto());
			page.getPageItems().add(item);
		}

		page.setTotalNumberOfPages(page.getPageItems().size());
		page.setTotalNumberOfPages(1);
		page.setThisPageNumber(1);

		PageOfItemsDto<FakeManagedCrudDto> d = page.toDto();

		Assert.assertEquals(page.getTotalNumberOfPages(),
				d.getTotalNumberOfPages());
		Assert.assertEquals(page.getThisPageNumber(), d.getThisPageNumber());
		Assert.assertEquals(page.getNumberOfItemsThisPage(),
				d.getNumberOfItemsThisPage());
		Assert.assertEquals(page.getPageItems().size(), d.getPageItems().size());
	}
}
