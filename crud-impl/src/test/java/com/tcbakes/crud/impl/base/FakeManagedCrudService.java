package com.tcbakes.crud.impl.base;

import static org.mockito.Mockito.mock;

import javax.persistence.EntityManager;

public class FakeManagedCrudService extends ManagedCrudService<FakeManagedCrud, FakeManagedCrudDto, Long> {

	
	Object somethingFakeCrudsNeed = new Object();

	public FakeManagedCrudService() {
		super();
		postContruct();
	}
	
	@Override
	protected Class<FakeManagedCrud> getManagedClass() {
		return FakeManagedCrud.class;
	}

	@Override
	protected EntityManager getAccessEntityManager() {
		return mock(EntityManager.class);
	}

	@Override
	protected EntityManager getItemEntityManager() {
		return mock(EntityManager.class);
	}
	
	
	@Override
	protected void postContruct() {
		//Create a bunch of Mock locators and entity managers
		this.setTxnAccess(mock(TransactionAccessLocal.class));
		this.setAccessFactory(mock(EntityFactory.class));
		this.setItemFactory(mock(EntityFactory.class));
		this.setItemLocator(mock(Locator.class));
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
}
