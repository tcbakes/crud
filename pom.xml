<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.tcbakes</groupId>
	<artifactId>crud</artifactId>
	<version>1.9.20-SNAPSHOT</version>
	<packaging>pom</packaging>

	<name>crud</name>
	<description>A generic CRUD pattern</description>
	<url>http://maven.apache.org</url>

	<scm>
		<connection>scm:git:https://${SCMuser}@bitbucket.org/tcbakes/crud.git</connection>
		<developerConnection>scm:git:https://${SCMuser}@bitbucket.org/tcbakes/crud.git</developerConnection>
	  <tag>crud-1.9.15</tag>
  </scm>
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<!-- Define the version of JBoss' Java EE 6 APIs and Tools we want to import. -->
		<version.org.jboss.bom>1.0.0.Final</version.org.jboss.bom>
		<maven.compiler.target>1.7</maven.compiler.target>
		<maven.compiler.source>1.7</maven.compiler.source>

	</properties>

	<repositories>
		<repository>
			<id>JBoss</id>
			<name>JBoss Release Repository</name>
			<url>https://repository.jboss.org/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>

	<distributionManagement>
		<repository>
			<id>repo-release</id>
			<name>repo-release</name>
			<url>http://sdgctgdevrepo.corp.intuit.net/nexus/content/repositories/ENG.CTG.Intuit-Releases</url>
		</repository>
		<snapshotRepository>
			<id>repo-snapshot</id>
			<name>repo-snapshot</name>
			<url>http://sdgctgdevrepo.corp.intuit.net/nexus/content/repositories/ENG.CTG.Intuit-Snapshots</url>
			<uniqueVersion>true</uniqueVersion>
		</snapshotRepository>
	</distributionManagement>

	<modules>
		<module>crud-spec</module>
		<module>crud-impl</module>
    	<module>crud-tools</module>
  </modules>

	<dependencyManagement>
		<dependencies>


			<!--Inter-project dependencies are listed here. Listing the sub projects 
				in the dependency management section allows us to control and coordinate 
				the version of a project used by another project (from implementation to 
				interface, for instance). This will ensure that every project uses the equivalent 
				version of another project. IMPORTANT: When you create a new project, add 
				it to this list. -->
			<dependency>
				<groupId>com.tcbakes</groupId>
				<artifactId>crud-spec</artifactId>
				<version>${project.version}</version>
			</dependency>
			
			<dependency>
				<groupId>com.tcbakes</groupId>
				<artifactId>crud-tools</artifactId>
				<version>${project.version}</version>
			</dependency>


			<!-- JBoss distributes a complete set of Java EE 6 APIs including a Bill 
				of Materials (BOM). A BOM specifies the versions of a "stack" (or a collection) 
				of artifacts. We use this here so that we always get the correct versions 
				of artifacts. Here we use the jboss-javaee-6.0-with-hibernate stack (you 
				can read this as the JBoss stack of the Java EE Web Profile 6 APIs with extras 
				from the Hibernate family of projects) -->
			<dependency>
				<groupId>org.jboss.bom</groupId>
				<artifactId>jboss-javaee-6.0-with-hibernate</artifactId>
				<version>${version.org.jboss.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>4.11</version>
			</dependency>

		</dependencies>
	</dependencyManagement>

	<dependencies>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.6</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.6</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.12</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<version>1.9.5</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.9.5</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.1.2</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<phase>deploy</phase>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.8.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<phase>deploy</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<!-- explicitly define maven-deploy-plugin after other to force exec 
					order -->
				<artifactId>maven-deploy-plugin</artifactId>
				<version>2.7</version>
				<executions>
					<execution>
						<id>deploy</id>
						<phase>deploy</phase>
						<goals>
							<goal>deploy</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.1</version>
            </plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>disable-java8-doclint</id>
			<activation>
				<jdk>[1.8,)</jdk>
			</activation>
			<properties>
				<additionalparam>-Xdoclint:none</additionalparam>
			</properties>
		</profile>
	</profiles>
</project>
