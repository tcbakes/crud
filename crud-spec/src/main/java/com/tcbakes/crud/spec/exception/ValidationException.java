package com.tcbakes.crud.spec.exception;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

/**
 * The nature of the structure of the request is invalid (field missing,
 * something wasn't unique and should have been, etc). When possible, an error
 * message will attempt to explain the cause of the issue. This is equivalent to
 * a HTTP 400 response code
 * 
 * @see javax.ws.rs.core.Response.Status#BAD_REQUEST
 * @author Tristan Baker
 * 
 */
public class ValidationException extends AbstractSpecException {

    private static final long serialVersionUID = 1L;
    
    private Map<String, List<String>> fieldErrors;

    public ValidationException() {
        super();
    }
    
    public ValidationException(String msg, Map<String, List<String>> fieldErrors) {
        super(msg);
        this.fieldErrors = fieldErrors;
    }

    @Override
    public Status getHttpStatusCode() {
        return Status.BAD_REQUEST;
    }
    
    @Override
    public SimpleExceptionDetails toDetail() {
        return new ValidationDetails(this.fieldErrors);
    }
}
