package com.tcbakes.crud.spec.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.core.Response.Status;

@ApplicationException(inherited = true, rollback = true)
public abstract class AbstractSpecException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AbstractSpecException() {
        super();
    }

    public AbstractSpecException(String msg) {
        super(msg);
    }

    public AbstractSpecException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public abstract Status getHttpStatusCode();
    
    public SimpleExceptionDetails toDetail() {
        return new SimpleExceptionDetails(getMessage());
    }

}
