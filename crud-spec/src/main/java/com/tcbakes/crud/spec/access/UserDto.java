package com.tcbakes.crud.spec.access;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import com.tcbakes.crud.spec.base.ManagedDto;

/**
 * A User represents some external entity attempting to use the data and
 * services provided by the system. A User might represent an actual human
 * person, or it might represent some external system. Each unique kind of user
 * will be captured by implementing classes
 * 
 * @author Tristan Baker
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public abstract class UserDto extends ManagedDto<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    List<String> userGroups;

    /**
     * @return The list of named Groups to which the User belongs
     */
    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> groups) {
        this.userGroups = groups;
    }

}
