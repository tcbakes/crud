package com.tcbakes.crud.spec.base;

import com.tcbakes.crud.spec.access.AuthenticateServiceREST;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;

/**
 * Defines a generic contract for deleting the state of a system item
 * 
 * @param <D>
 *            The {@link Dto} being managed
 * @param <I>
 *            The type of the {@link Dto}'s primary id.
 * 
 * @author Tristan Baker
 */
public interface Deletes<D extends Dto<I>, I> {

	/**
	 * Removes the item from the system. Access is granted/denied via the
	 * tokenId, which identifies the actor performing the operation.
	 * 
	 * @param id
	 *            The id of the item to remove
	 * @param tokenId
	 *            The reference to the token granting access to perform this
	 *            operation
	 * @throws BadRequestException
	 *             If id is null
	 * @throws AccessForbiddenException
	 *             If the AccessToken referenced by tokenId does not grant
	 *             access to this operation
	 * @throws ServiceException
	 *             All other errors
	 * @see AuthenticateServiceREST#authenticate(com.tcbakes.crud.spec.access.CredentialDto)
	 */
	public abstract void delete(I id, String tokenId)
			throws BadRequestException, AccessForbiddenException,
			ServiceException;
}
