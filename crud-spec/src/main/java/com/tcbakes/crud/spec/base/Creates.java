package com.tcbakes.crud.spec.base;

import com.tcbakes.crud.spec.access.AuthenticateServiceREST;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.ValidationException;

/**
 * Defines a generic contract for creating a system item.
 *
 * @param <T> The {@link Dto} being managed
 * @param <I> The id used to identify the {@link Dto}
 * 
 * @author Tristan Baker
 */
public interface Creates<T extends Dto<I>, I> {

	/**
	 * Creates a new item in the system. Access is granted/denied via
	 * the tokenId, which identifies the actor performing the operation.
	 * 
	 * @param d
	 *            The item to create
	 * @param tokenId
	 *            The reference to the token granting access to perform this
	 *            operation
	 * @return The created item
	 * @throws BadRequestException
	 *             If d is null
	 * @throws NotFoundException
	 *             If d references another item that is not found
	 * @throws ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws AccessForbiddenException
	 *             If the AccessToken referenced by tokenId does not grant
	 *             access to this operation
	 * @throws ServiceException
	 *             Any other error
	 * @see AuthenticateServiceREST#authenticate(com.tcbakes.crud.spec.access.CredentialDto)
	 */
	public abstract T create(T d, String tokenId)
			throws BadRequestException, NotFoundException, ValidationException,
			AccessForbiddenException, ServiceException;
}
