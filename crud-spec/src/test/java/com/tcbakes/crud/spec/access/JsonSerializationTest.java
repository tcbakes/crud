package com.tcbakes.crud.spec.access;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

public class JsonSerializationTest {

	@Test
	public void credentialSerializationTest() throws JsonParseException, JsonMappingException, IOException{
		
		String jsonInput =   
			      "{\"class\":\"com.tcbakes.crud.spec.access.FakeCredential\",\"username\":\"foo\",\"password\":\"bar\"}";
		
		ObjectMapper mapper = new ObjectMapper();  
		FakeCredential cred = mapper.readValue(jsonInput, FakeCredential.class);
		
		Assert.assertEquals("foo", cred.getUsername());
	    
	}
	
	@Test
	public void userSerializationTest() throws JsonParseException, JsonMappingException, IOException{
		
		String jsonInput =   
			      "{\"class\":\"com.tcbakes.crud.spec.access.FakeUser\",\"id\":\"123\",\"username\":\"foo\"}";
		
		ObjectMapper mapper = new ObjectMapper();  
		FakeUser user = mapper.readValue(jsonInput, FakeUser.class);

		Assert.assertEquals(new Long(123), user.getId());
		Assert.assertEquals("foo", user.getUsername());
	    
	}
}
